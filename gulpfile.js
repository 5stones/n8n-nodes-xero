const { watch, src, dest } = require('gulp');

const assets = 'src/**/*.svg';
function copyIcons() {
	return src('src/**/*.svg')
		.pipe(dest('dist'));
}

function dev() {
	watch('src/**/*', copyIcons);
}

exports.watch = dev;
exports.default = copyIcons;
