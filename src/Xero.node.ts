import { IExecuteFunctions, BINARY_ENCODING } from 'n8n-core';
import {
	IDataObject,
	ILoadOptionsFunctions,
	INodeExecutionData,
	INodeType,
	INodeTypeDescription,
	INodePropertyOptions,
	IBinaryData,
} from 'n8n-workflow';

import { request, requestPaginated, RequestOptions } from './utils';

export class Xero implements INodeType {
	description: INodeTypeDescription = {
	  displayName: 'Xero (simple)',
	  name: 'xero-simple',
		icon: 'file:xero.svg',
		group: ['output'],
	  version: 1,
	  description: 'Allows requests to the Xero API',
	  defaults: {
	    name: 'Xero',
	    color: '#0D52FF',
	  },
	  inputs: ['main'],
	  outputs: ['main'],
	  credentials: [
	    {
	      name: 'xeroOAuth2Api',
	      required: true,
	    },
	  ],
	  properties: [
			{
				displayName: 'Request Method',
				name: 'requestMethod',
				type: 'options',
				options: [
					{
						name: 'DELETE',
						value: 'DELETE',
					},
					{
						name: 'GET',
						value: 'GET',
					},
					{
						name: 'GET (Paginated)',
						value: 'GET_ALL',
					},
					{
						name: 'HEAD',
						value: 'HEAD',
					},
					{
						name: 'PATCH',
						value: 'PATCH',
					},
					{
						name: 'POST',
						value: 'POST',
					},
					{
						name: 'PUT',
						value: 'PUT',
					},
				],
				default: 'GET',
				description: 'The request method to use.',
			},
			{
				displayName: 'Organization/Tenant ID',
				name: 'organizationId',
				type: 'options',
				typeOptions: {
					loadOptionsMethod: 'getTenants',
				},
				default: '',
				description: 'The unique identfier of the organization/tenant.',
				required: true,
			},
			{
				displayName: 'Endpoint',
				name: 'endpoint',
				type: 'string',
				default: '',
				placeholder: '/Invoices',
				description: 'The endpoint to make the request to.',
				required: true,
			},
			{
				displayName: 'Response Format',
				name: 'responseFormat',
				type: 'options',
				options: [
					{
						name: 'JSON',
						value: 'json',
					},
					{
						name: 'String',
						value: 'string',
					},
				],
				default: 'json',
				description: 'The format in which the data gets returned from the URL.',
			},
			{
				displayName: 'Send Binary Data',
				name: 'sendBinaryData',
				type: 'boolean',
				displayOptions: {
					show: {
						requestMethod: [
							'PATCH',
							'POST',
							'PUT',
						],
					},
				},
				default: false,
				description: 'If binary data should be send as body.',
			},
			{
				displayName: 'Binary Property',
				name: 'binaryPropertyName',
				type: 'string',
				required: true,
				default: 'data',
				displayOptions: {
					hide: {
						sendBinaryData: [
							false,
						],
					},
					show: {
						requestMethod: [
							'PATCH',
							'POST',
							'PUT',
						],
					},
				},
				description: `Name of the binary property which contains the data for the file to be uploaded.<br />
							For Form-Data Multipart, multiple can be provided in the format:<br />
							"sendKey1:binaryProperty1,sendKey2:binaryProperty2`,
			},
			{
				displayName: 'Body Parameters',
				name: 'body',
				type: 'json',
				displayOptions: {
					hide: {
						sendBinaryData: [
							true,
						],
					},
					show: {
						requestMethod: [
							'PATCH',
							'POST',
							'PUT',
						],
					},
				},
				default: '',
				description: 'Body parameters as JSON.',
			},
			{
				displayName: 'Query Parameters',
				name: 'query',
				type: 'json',
				default: '',
				description: 'Query parameters as JSON (flat object).',
			},
			{
				displayName: 'Headers',
				name: 'headers',
				type: 'json',
				default: '',
				description: 'Query parameters as JSON (flat object).',
			},
			{
				displayName: 'Additional Fields',
				name: 'additionalFields',
				type: 'collection',
				placeholder: 'Add Field',
				default: {},
				options: [
					{
						displayName: 'Branding Theme ID',
						name: 'brandingThemeID',
						type: 'options',
						typeOptions: {
							loadOptionsMethod: 'getBrandingThemes',
							loadOptionsDependsOn: [
								'organizationId',
							],
						},
						default: '',
					},
				]
			},
	  ],
	};

	methods = {
		loadOptions: {
			// Get all the tenants to display them to user so that he can
			// select them easily
			async getTenants(this: ILoadOptionsFunctions): Promise<INodePropertyOptions[]> {
				const returnData: INodePropertyOptions[] = [];
				const options: RequestOptions = {
					executionContext: this,
					method: 'GET',
					endpoint: 'https://api.xero.com/connections',
				};
				const tenants = await request(options);
				for (const tenant of tenants) {
					const tenantName = tenant.tenantName;
					const tenantId = tenant.tenantId;
					returnData.push({
						name: tenantName,
						value: tenantId,
					});
				}

				return returnData;
			},
			// Get all the brading themes to display them to user so that he can
			// select them easily
			async getBrandingThemes(this: ILoadOptionsFunctions): Promise<INodePropertyOptions[]> {
				const tenantId = this.getCurrentNodeParameter('organizationId') as string;
				const returnData: INodePropertyOptions[] = [];
				const options: RequestOptions = {
					executionContext: this,
					method: 'GET',
					endpoint: '/BrandingThemes',
					tenantId,
				};
				const { BrandingThemes: themes } = await request(options);
				for (const theme of themes) {
					const themeName = theme.Name;
					const themeId = theme.BrandingThemeID;
					returnData.push({
						name: themeName,
						value: themeId,
					});
				}
				return returnData;
			},
		},
	};

	async execute(this: IExecuteFunctions): Promise<INodeExecutionData[][]> {
		const items = this.getInputData();
		const returnData: IDataObject[] = [];

		for (let i = 0; i < items.length; i++) {
			const item = items[i];
			const tenantId = this.getNodeParameter('organizationId', i) as string;
			const additionalFields = this.getNodeParameter('additionalFields', i) as IDataObject;
			const method = this.getNodeParameter('requestMethod', i) as string;
			const endpoint = this.getNodeParameter('endpoint', i) as string;
			const body = this.getNodeParameter('body', i, null);
			const qs = this.getNodeParameter('query', i) || {};
			let headers = this.getNodeParameter('headers', i) || {};
			const sendBinaryData = this.getNodeParameter('sendBinaryData', i, false) as boolean;
			const binaryPropertyName = this.getNodeParameter('binaryPropertyName', i, '') as string;
			const responseFormat = this.getNodeParameter('responseFormat', i) as string;

			if (typeof headers === 'string') {
				headers = JSON.parse(headers);
			}

			const options: RequestOptions = {
				executionContext: this,
				method,
				endpoint,
				qs,
				headers,
				tenantId,
			};

			if (sendBinaryData) {
				if (item.binary === undefined) {
					throw new Error('No binary data exists on item!');
				}
				if (item.binary[binaryPropertyName] === undefined) {
					throw new Error(`No binary data property "${binaryPropertyName}" does not exists on item!`);
				}

				const binaryProperty = item.binary[binaryPropertyName] as IBinaryData;
				options.body = Buffer.from(binaryProperty.data, BINARY_ENCODING);
				options.json = false;
			} else if (body) {
				options.body = body;
			}

			if (additionalFields.brandingThemeID) {
				if (!options.body) {
					options.body = {};
				}
				options.body.brandingThemeID = additionalFields.brandingThemeID;
			}

			let response;
			if (method !== 'GET_ALL') {
				response = await request(options);
			} else {
				options.method = 'GET';
				response = await requestPaginated(options);
			}

			if (responseFormat === 'json' && typeof response === 'string') {
				response = JSON.parse(response.replace(/^\uFEFF/gm, ''));
			}

			returnData.push(response);
		}

		return [this.helpers.returnJsonArray(returnData)];
	}
}
