import { IExecuteFunctions, ILoadOptionsFunctions } from 'n8n-core';

import { request, RequestOptions } from './request';

export async function requestPaginated(options: RequestOptions): Promise<any> {
	const result: any[] = [];
	let response;
	options.qs.page = 1;
  const propertyName = options
    .endpoint
    .split('/')
		// filter out empty strings and select the first element
    .filter(item => !!item)[0]
  ;

	let more = true;
	while (more) {
		response = await request(options);
		options.qs.page++;
		result.push(...response[propertyName]);

		if (response[propertyName].length === 0) {
			more = false;
		}
	}

	return { [propertyName]: result };
}
