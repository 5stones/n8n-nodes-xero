import { IDataObject } from 'n8n-workflow';
import {
	IExecuteFunctions,
	IExecuteSingleFunctions,
	ILoadOptionsFunctions,
} from 'n8n-core';

export interface RequestOptions {
	executionContext: IExecuteFunctions | IExecuteSingleFunctions | ILoadOptionsFunctions;
	method: string;
	endpoint: string;
	body?: any;
	qs?: any;
	headers?: any;
	tenantId?: string;
	json?: boolean;
}

export async function request({
  executionContext,
  method,
  endpoint,
  body = {},
  qs = {},
  headers = {},
	tenantId,
	json = true,
}: RequestOptions): Promise<any> {
	const options = {
		headers: {
      'Content-Type': 'application/json',
			accept: 'application/json,text/*;q=0.99',
      ...headers
    },
		method,
		body,
		qs,
		uri: endpoint.includes('http') ? endpoint : `https://api.xero.com/api.xro/2.0${endpoint}`,
		json,
		gzip: true,
	};
	if (json === false) {
		delete options.headers['Content-Type'];
	}

  // assign a tenant id if one exists
  if (tenantId) {
    options.headers['xero-tenant-id'] = tenantId;
  }

  // delete the body if it's empty
  if (options.json && Object.keys(options.body).length === 0) {
    delete options.body;
  }

	if (Object.keys(options.qs).length === 0) {
    delete options.qs;
  }

	const sleep = (ms: number) => {
	  return new Promise(resolve => setTimeout(resolve, ms));
	};

	for (let retryCount = 0; retryCount < 5; retryCount++) {
		try {
			//@ts-ignore
			return await executionContext.helpers.requestOAuth2.call(executionContext, 'xeroOAuth2Api', options);
		} catch (error: any) {
			if (error.statusCode === 429) {
				const problem = error.response.headers["x-rate-limit-problem"];

				if (problem === 'concurrent') {
					await sleep(5000);
				} else if (problem === 'minute') {
					const tryAfter = parseInt(error.response.headers["retry-after"]) * 1000;
					await sleep(tryAfter);
				} else {
					throw new Error(`Rate limit problem ${problem} occured for endpoint ${endpoint}`);
				}
			} else if (error.response && error.response.body && error.response.body.Message) {
				parseError(error.response.body, error.response.status);
			} else if (error.response && error.response.data && error.response.data.Message){
				parseError(error.response.data, error.response.status);
			} else {
				throw error;
			}
		}
	}

	throw new Error(`Too many retries at endpoint ${endpoint}`);
}

function parseError(data: any, errorCode: string) {
	let errorMessage;

	errorMessage = data.Message;

	if (data.Elements) {
		const elementErrors = [];
		for (const element of data.Elements) {
			elementErrors.push(element.ValidationErrors.map((error: IDataObject) =>  error.Message).join('|||'));
		}
		errorMessage = elementErrors.join('---');
	}

	// Try to return the error prettier
	throw new Error(`Xero error response [${errorCode}]: ${errorMessage}`);
}
